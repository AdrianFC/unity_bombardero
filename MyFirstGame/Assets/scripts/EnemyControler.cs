﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyControler : MonoBehaviour
{
    // Start is called before the first frame update
    [Tooltip("Bomba que se lanza")]
    public GameObject bomb;
    void Start()
    {
        Bombard();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void Bombard()
    {
        if (bomb != null)
        {
            Instantiate(bomb);
        }
    }
}
