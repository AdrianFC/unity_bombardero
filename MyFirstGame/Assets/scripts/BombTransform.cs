﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombTransform : MonoBehaviour
{
    [Tooltip("velocidad de caída bomba")] 
    public float speed = 0.2f;
    // Start is called before the first frame update
    void Start()
    {
        //Application.targetFrame = 3;
    }

    // Update is called once per frame
    void Update()
    {
       
      Transform t = GetComponent<Transform>();
        Vector3 pos = t.position;
        
        pos.y = pos.y - speed;
        t.position = pos;

    }
}
